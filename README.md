Post Youtube Videos to Subreddit
================================

Installations
-------------

This script requires python 2.7, praw, and the google python api.

To install praw type in your command line:

    pip install praw

To install the google api type in your command line:

    pip install google-api-python-client

Setting Up Your Google Dev Account
----------------------------------

Then you must get your developer key from google, to do that go to:

[console.developers.google.com/project](https://console.developers.google.com/project)

And create a project.

From the console now, go to APIs & auth, and navigate to the APIs screen.

Click on YouTube Data API

![Youtube data api](http://i.imgur.com/jN79VWq.png)

And click enable API at the top.

![Activate Youtube API](http://i.imgur.com/9LvPHgQ.png)

From there click on credentials and you'll see in the Public API Access area
and line called API key, copy that into the variable DEVELOPER_KEY in
youtube_submitter_bot.py.

![Developer key](http://i.imgur.com/MR9recA.png)

Getting Channel IDs For Searching
---------------------------------

Google's API only allows specific channel searching with only the channel ID.
To get the channel ID, open this link in your browser:

    https://www.googleapis.com/youtube/v3/channels?key={YOUR_API_KEY}&forUsername={Channel Name}&part=id

Replace the areas in the bracket with your API key from earlier, and with the
channel you want to find. And it should show you a line that says id. That's
the channels ID.

![Channel ID](http://i.imgur.com/zhbMOZT.png)

Find all the channels you want and add them as string to the list CHANNEL_IDS in the
youtube_submitter_bot.py file.

Then add your Reddit Username and Password to the variables and it's ready to run.

Options
-------

If you want to change what css class is given to the flair you can change the
FLAIR_CSS_CLASS variable.

Also, if you want custom flairs for each video channel, switch the APPEND_CHANNEL_NAME
variable to True. This will add the channel name to your css class, with a hyphen.

   (css-class)-(channel-name)

 It will be and all lowercase and hyphenate any spaces in the name.

 For the flairs to get added, all names and classes must be added via the
 edit flair menu by a mod, or the flairs won't get added to the posts.
