#-------------------------------------------------------------------------------
# Youtube Submitter Bot
# /u/EDGYALLCAPSUSERNAME
#-------------------------------------------------------------------------------
from apiclient.discovery import build
from apiclient.errors import HttpError
import praw
import time
import urllib2

DEVELOPER_KEY = ""
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

REDDIT_USERNAME = ''
REDDIT_PASS = ''
SUBREDDIT_NAME = ''

# Change this if you want to append the channel name to the end of the class
APPEND_CHANNEL_NAME = False
FLAIR_CSS_CLASS = "video"

# Insert all the strings of the channel ids you want it to search
CHANNEL_IDS = []

LINK_TEMPLATE = 'https://www.youtube.com/watch?v={}'

def post_to_reddit(r, sub, video):
    video_title = video[0].encode('ascii', 'ignore')
    channel_name = video[1].encode('ascii', 'ignore')
    video_id = video[2].encode('ascii', 'ignore')
    link = LINK_TEMPLATE.format(video_id)

    if APPEND_CHANNEL_NAME:
        flair_css_class = FLAIR_CSS_CLASS + "-" + channel_name.replace(' ', '-').lower()
    else:
        flair_css_class = FLAIR_CSS_CLASS

    while True:
        try:
            submission = r.submit(sub, video_title, text=None, url=link, captcha=None)
            submission.set_flair(flair_css_class = flair_css_class,
                                 flair_text = channel_name)
            print "Posted video."
            break
        except praw.errors.AlreadySubmitted:
            print "Already submitted video, continuing..."
            break
        except urllib2.HTTPError, e:
            if e.code in [429, 500, 502, 503, 504]:
                print "Reddit is down (Error: {}), sleeping...".format(e.code)
                time.sleep(60)

def youtube_search(channelId):
    youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
                    developerKey = DEVELOPER_KEY)

    search_response = youtube.search().list(
                        channelId = channelId,
                        order = 'date',
                        part = "id, snippet",
                        maxResults = 10, # Change if you want to search more videos at a time.
                        type = 'video').execute()

    videos = []
    for search_result in search_response.get("items", []):
        videos.append([search_result['snippet']['title'],
                       search_result['snippet']['channelTitle'],
                       search_result['id']['videoId']])


    return videos

def main():
    r = praw.Reddit(user_agent = 'Youtube_Poster v0.1')
    r.login(REDDIT_USERNAME, REDDIT_PASS, disable_warning = True)
    sub = r.get_subreddit(SUBREDDIT_NAME)

    for channel in CHANNEL_IDS:
        while True:
            try:
                videos = youtube_search(channel)
                break
            except HttpError, e:
                print "An HTTP error {} occured in:\n{}".format(e.resp.status,
                                                                e.content)
                print "Sleeping and then trying again..."
                time.sleep(120)

        for video in videos:
            post_to_reddit(r, sub, video)

if __name__ == "__main__":
    main()
