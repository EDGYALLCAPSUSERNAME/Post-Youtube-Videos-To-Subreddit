#-------------------------------------------------------------------------------
# Youtube Submitter Bot
# /u/EDGYALLCAPSUSERNAME
#-------------------------------------------------------------------------------
from apiclient.discovery import build
from apiclient.errors import HttpError
import praw
import time

DEVELOPER_KEY = ""
YOUTUBE_API_SERVICE_NAME = "youtube"
YOUTUBE_API_VERSION = "v3"

REDDIT_USERNAME = ''
REDDIT_PASS = ''
SUBREDDIT_NAME = ''

# Change this if you want to append the channel name to the end of the class
APPEND_CHANNEL_NAME = False
FLAIR_CSS_CLASS = "video"

# Insert all the strings of the channel ids you want it to search
CHANNEL_IDS = []

LINK_TEMPLATE = 'https://www.youtube.com/watch?v={}'

def post_to_reddit(r, sub, video):
    video_title = str(video[0])
    channel_name = str(video[1])
    video_id = str(video[2])
    link = LINK_TEMPLATE.format(video_id)

    if APPEND_CHANNEL_NAME:
        flair_css_class = "{}-{}".format(FLAIR_CSS, channel_name.replace(' ', '-')).encode('utf-8')
    else:
        flair_css_class = FLAIR_CSS

    while True:
        try:
            submission = r.submit(sub, video_title, text=None, url=link, captcha=None)
            submission.set_flair(flair_css_class = flair_css_class,
                                 flair_text = channel_name)
            print("Posted video.")
            break
        except praw.errors.AlreadySubmitted:
            print ("Already submitted video, continuing...")
            break

def youtube_search(channelId):
    youtube = build(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION,
                    developerKey = DEVELOPER_KEY)

    search_response = youtube.search().list(
                        channelId = channelId,
                        order = 'date',
                        part = "id, snippet",
                        maxResults = 10,
                        type = 'video').execute()

    videos = []

    for search_result in search_response.get("items", []):
        videos.append([search_result['snippet']['title'],
                       search_result['snippet']['channelTitle'],
                       search_result['id']['videoId'],
                       search_result['snippet']['publishedAt']])


    return videos


def main():
    r = praw.Reddit(user_agent = 'Youtube_Poster v0.1')
    r.login(REDDIT_USERNAME, REDDIT_PASS, disable_warning = True)
    sub = r.get_subreddit(SUBREDDIT_NAME)

    for channel in CHANNEL_IDS:
        while True:
            try:
                videos = youtube_search(channel)
                break
            except HttpError as e:
                print("An HTTP error {} occured in:\n{}".format(e.resp.status,
                                                                e.content))
                print("Sleeping and then trying again...")
                time.sleep(120)

        # videos = [x for x in videos if x is not None]
        for video in videos:
            post_to_reddit(r, sub, video)

if __name__ == "__main__":
    main()
